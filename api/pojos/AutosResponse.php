<?php


class AutosResponse{
    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $nombre;

    /**
     *
     * @var integer
     */
    public $idmarca;

    public $nombremarca;

    /**
     *
     * @var integer
     */
    public $precio;

    /**
     *
     * @var integer
     */
    public $kilometraje;
}