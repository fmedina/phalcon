<?php
/**
 * Local variables
 * @var \Phalcon\Mvc\Micro $app
 */
define('ROUTES_PATH', realpath('..') . '\\routes\\');
error_reporting(E_ALL);
ini_set('display_errors', 1);

/**
 * Add your routes here
 */

 ///////////////////////////////////////////////////////
 /// /rutas
 ///////////////////////////////////////////////////////
include ROUTES_PATH . 'autos.php';
include ROUTES_PATH . 'marcas.php';

function debug($e,$m){
    $debug = true;
    if($debug){
        return " DEBUG: ".$e->getMessage();
    }else{
        return $m;
    }
}

/**
 * Not found handler
 */
$app->notFound(function () use ($app) {
    $app->response->setStatusCode(404, "Not Found")->sendHeaders();
});