<?php
use Phalcon\Http\Response;

$app->get('/marcas', function() use ($app){
    $response = new Response();
    $data = array();
    try{
        $marcas = Marcas::find();
        foreach($marcas as $marca){
            $marcasResponse = new MarcasResponse();
            $marcasResponse->id = intval($marca->id);
            $marcasResponse->nombre = $marca->nombre;
            array_push($data, $marcasResponse);
        }
        if($marcas->count() > 0){
            $response->setJsonContent($data);
        }else{
            $app->response->setStatusCode(204, "No Content")->sendHeaders();
        }
        return $response;
    }catch(\Exception $ex){
         $app->response->setStatusCode(500, debug($e,"Internal Server Error"))->sendHeaders();
        return $response;
    };
});