var app = angular.module('phalcon',['ngRoute','ngResource']);

app.service("ServiceHome", function($q, $http){
    this.getAllMarcas = function (url) {
        var defered = $q.defer();
        var promise = defered.promise;
        $http.get("api/marcas").then(function successCallback(data){
            var _data = { "data": data.data, "status": data.status };
		    defered.resolve(_data);
        }, function errorCallback(error){
            var _edta = { message: error, status: status };
		    if (status === 401) {
                notification.notify( 'error', 'Credenciales necesarias' );
		    }
		    defered.reject(_edta);
        });
        return promise;
    };
    this.getAllAutos = function (url) {
        var defered = $q.defer();
        var promise = defered.promise;
        $http.get("api/autos").then(function successCallback(data){
            var _data = { "data": data.data, "status": data.status };
		    defered.resolve(_data);
        }, function errorCallback(error){
            var _edta = { message: error, status: status };
		    if (status === 401) {
                notification.notify( 'error', 'Credenciales necesarias' );
		    }
		    defered.reject(_edta);
        });
        return promise;
    };
    this.postNew = function (data, type, url) {
        var defered = $q.defer();
        var promise = defered.promise;
        var method = 'POST';
        switch (type) {
            case 2:
                method = "PUT";
                break;
            case 3:
                method = "DELETE";
                break;
        }
        var req = {
            method: method,
            url: url,
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            data: data,
            withCredentials: true
        }
        $http(req, { withCredentials: true })
        .then(function successCallback(data){
            var _data = { "data": data, "status": data.status };
            defered.resolve(_data);
        }, function errorCallback(error){
            var _edta = { message: error, status: error.status };
            if (error.status === 401) {
                /*store.removeAll();
                window.location.href = "./";*/
                notification.notify( 'error', 'Credenciales incorrectas' );
            }
            defered.reject(_edta);
        });
        return promise;
    };
});
app.controller("ControllerHome", function($scope, ServiceHome, $filter){
    $scope.filterMarcas = function (object, id) {
        return $filter('filter')(object, function (d) { return d.id === id })[0];
    }

    $scope.auto = {id: "", nombre: "", idmarca: "", precio: "", kilometraje: "", nombremarca:""};
    $scope.marcas = [];

    ServiceHome.getAllMarcas()
    .then(function(response){
        $scope.marcas = response.data;
    })
    ["catch"](function(ex){
        console.log(ex);
    });

    $scope.autos = [];

    ServiceHome.getAllAutos()
    .then(function(response){
        $scope.autos = response.data;
    })
    ["catch"](function(ex){
        console.log(ex);
    });
    $scope.type = 1;
    $scope.saveAuto = function(){
        ServiceHome.postNew($scope.auto,$scope.type, "api/autos")
        .then(function(data){
            $scope.type = 1;
             $scope.auto = {id: "", nombre: "", idmarca: "", precio: "", kilometraje: "", nombremarca:""};
           ServiceHome.getAllAutos()
            .then(function(response){
                $scope.autos = response.data;
            })
            ["catch"](function(ex){
                console.log(ex);
            });
        })
        ['catch'](function(ex){
            console.log("Error al guardar el auto")
        });
    }

    $scope.editar = function(auto){
        $scope.auto = auto;
        $scope.type = 2;
    }
});



app.config(function($routeProvider){
    $routeProvider
    .when('/', {
        templateUrl: 'home.html',
        controller: 'ControllerHome'
    })
    .otherwise({
        redirectTo: '/'
    });
});