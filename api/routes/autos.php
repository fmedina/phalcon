<?php
use Phalcon\Http\Response;
$app->get('/autos', function () use ($app) {
    $response = new Response();
    $data = array();
    try{
        $autos = Autos::find();
        foreach($autos as $auto){
            $autosResponse = new AutosResponse();
            $autosResponse->id = intval($auto->id);
            $autosResponse->nombre = $auto->nombre;
            $autosResponse->idmarca = intval($auto->idmarca);
            $autosResponse->nombremarca = Marcas::findFirst($auto->idmarca)->nombre;
            $autosResponse->precio = intval($auto->precio);
            $autosResponse->kilometraje = intval($auto->kilometraje);
            array_push($data, $autosResponse);
        }
        if($autos->count() > 0){
            $response->setJsonContent($data);
        }else{
            $app->response->setStatusCode(500, debug($e,"Internal Server Error"))->sendHeaders();
        }
        return $response;
    }catch(\Exception $e){
        $app->response->setStatusCode(500, debug($e,"Internal Server Error"))->sendHeaders();
        return $response;
    }
});

$app->post('/autos', function() use ($app){
    $response = new Response();
    $autosRq = $app->request->getJsonRawBody();
    try{
        $autos = new Autos();
        $autos->nombre = $autosRq->nombre;
        $autos->idmarca = $autosRq->idmarca;
        $autos->precio = $autosRq->precio;
        $autos->kilometraje = $autosRq->kilometraje;
        $isCommit = $autos->save($autos);
        if($isCommit){
            $response->setJsonContent(array('status' => '200', 'commit'=>$isCommit));
        }else{
            $response->setJsonContent(406, 'Not Acceptable')->sendHeaders();;
        }
        return $response;
    }catch(\Exception $e){
        $app->response->setStatusCode(500, debug($e,"Internal Server Error"))->sendHeaders();
        return $response;
    }
});

$app->put('/autos', function() use ($app){
    $response = new Response();
    $autosRq = $app->request->getJsonRawBody();
    try{
        $autos = Autos::findFirst($autosRq->id);
        $autos->id = $autosRq->id;
        $autos->nombre = $autosRq->nombre;
        $autos->idmarca = $autosRq->idmarca;
        $autos->precio = $autosRq->precio;
        $autos->kilometraje = $autosRq->kilometraje;
        $isCommit = $autos->save($autos);
        if($isCommit){
            $response->setJsonContent(array('status' => '200', 'commit'=>$isCommit));
        }else{
            $response->setJsonContent(406, 'Not Acceptable')->sendHeaders();;
        }
        return $response;
    }catch(\Exception $e){
        $app->response->setStatusCode(500, debug($e,"Internal Server Error"))->sendHeaders();
        return $response;
    }
});
